<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="./css/reset.css" type="text/css" media="all">
  <link rel="stylesheet" href="./css/layout.css" type="text/css" media="all">
  <link rel="stylesheet" href="./css/style.css" type="text/css" media="all">
  <!--[if lt IE 9]>
    <script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
  	<script type="text/javascript" src="js/html5.js"></script>
  <![endif]-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37814978-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body id="page2">
<div class="body1">
			<!--header -->
			
				
                <div class="main">
                <div class="wrapper">
                    <nav>
                        <ul id="top_nav">
                            <li><a href="#"></a></li>
                            <li><a href="#"></a></li>
                            <li><a href="#"></a></li>
                            <li class="bg_none"><a href="#"></a></li>
                        </ul>
                    </nav>
                    <div id="date"></div>
                </div>
                </div>
           
            <div class="main">
            <div class="pad bg">
            <header>
            
                <div class="wrapper">
                    <div id='slogan'>loft events<span>054-2512-512</span></div>
                    <h1><a href="index.html" id="logo"><img src="./images/logo_lofte.png" alt="Events Loft" height="80px" width="190px"/>  <span></span></a></h1>
                </div>
                <nav>
                    <ul id="menu">
                        <li><a href="contact.php">&nbsp;&nbsp;צור קשר&nbsp;&nbsp;</a></li>
                        <li><a href="about.php">אודות הLoft</a></li>
                        <li><a href="gallery.php">&nbsp;&nbsp;&nbsp; גלריה &nbsp;&nbsp;&nbsp;</a></li>
                        <li><a href="BacheloretteParty.php">מסיבת רווקות</a></li>
                        <li><a href="BachelorParty.php">מסיבת רווקים</a></li>
                        <li class="bg_none"><a href="index.php">&nbsp;&nbsp;דף הבית&nbsp;&nbsp;</a></li>
                    </ul>
				</nav>
			</header>
			<!--header end-->
			<!--content -->
			<section id="content">
				
					<div class="wrapper">
					<article class="col4">
						<h2>אודות - Loft Events </h2>
						<div class="wrapper">
							<figure class="left marg_right1"><img alt="" src="./images/about1.jpg"></figure>
							<p class="font1">לופט איוונטס לאירועים יוקרתיים ויחודיים</p>
							<p class="color1 pad_bot1"> 
								לופט ששטחו כ-100 מטרים, המכיל עד 40 אנשים !
								לופט איוונס מיועד לכל מסיבה ולכל מי שמחפש מקום מעוצב ואיכותי לחגוג בו.
								הלופט ממוקם באיזור התעשיה בבת-ים, מקום דיסקרטי ושקט ניתן להשמיע מוזיקה 
								כל שעות הפעילות ללא הפרעה וטענה.
								בכל אירוע הלופט מאובטח בשומר.
								שטח חניה גדול ומואר ללא תשלום, וממש צמוד ללופט.
								.לופט איוונטס מעוצב ומאובזר ברמה הכי גבוהה שקיימת!!
							</p>
						</div><br />
						<p class="pad_bot1">מסיבות שיחרור, מסיבות הפתעה ,ערבי קריוקי, ימי הולדת לכל הגילאים ועוד...
							בלופט איוונטס,
							-ג'קוזי יוקרתי איכותי ומפנק, 8 מקומות ישיבה מרווחים, תאורת לדים וקביעת טמפרטורת המים.
							-מערכת סאונד, הגברה ותאורה מקצועית. (מיקסר, רמקולים, מקרופונים סאב וופר יוקרתיים)
							-מערכת קריוקי מקצועית, תיקייה עם 3000 שירי קריוקי, זוג מקרופונים אלחוטיים ואיכותיים,
							-מטבחון שירות הכולל, תמי 4 (מים קרים וחמים) מיקרוגל, מקרר גדול לאחסון, שיש ושולחן עבודה.
						</p> 
						<p class="color1 pad_bot1">-שולחן אבירים פינת אוכל לכ- 10 אנשים, ניתן למקם בכל מקום שתרצו.
							-עמדת DJ  בקומה השניה של הלופט.
							-מערכת ישיבה אקסקלוסיבית ונוחה במרכז הסלון.
							-פלזמה 50" בסלון עם כל ערוצי הספורט HD והסרטים של חברת YES.
							-קונסולת משחקים פלייסטיישן 3 עם המשחק שמשגע וגורם להמון שאטים – פרו אבולושן סוקר 2012
							-שולחן פוקר מקצועי במיוחד.
							-מקלחות ושירותים מרווחים אסטטיים ונקיים עם ניחוח מרענן.
							-אינטרנט אלחוטי מהיר גישה מלאה לכל האתרים, עם קוד גישה לסלולריים WIFI
							-מקרן  optuma HD  דרכו ניתן לצפות בשידורי הטלויזיה ובמערכת הקריוקי, וכמו כן להקרין מצגות
						 וסרטים אישיים.
						</p>
						
						<p class="pad_bot1">
						
						-פינת ברביקיו, ניתן לעשות על האש בחצר הלופט, (אנו מספקים מנגל גדול ושולחן שירות לבשרים)
						-חצר מסודרת ונינוחה, כיסאות ישיבה ושולחנות שירות, דשא סינטטי.
						-חדר שינה בקומה השניה של הלופט, חדר שינה דיסקרטי המאפשר פרטיות מיתר האנשים  
						 השוהים במסיבה, חדר השינה משקיף על הלופט כולו, מיטה זוגית מפנקת עם מצעים נעימים, 
						 טלויזיית LCD של חברת SHARP  עם אפשרות צפייה בסרטי DVD וערוצי YES HD
						אנו מציעים מגוון אטרקציות נוספות במחירים מוזלים כשרות ללקוחות LOFT EVENTS כגון:
						*בר משקאות לכל הערב עם מגוון משקאות תוצרת חוץ במחיר אטרקטיבי,
						*תמונות מגנטים – תמונות איכותיות וגדולות במיוחד במחיר מיוחד.
						*בר מתוקים – מעוצבים וטריים מתאים למסיבות ימי הולדת ורווקות.
						*DJ  מקצועי להפעלה רציפה של שירים.
						* מפעיל קריוקי מקצועי.
						*טיפולי מסאג' מקצועיים, במחיר מיוחד במינו, מתאים במיוחד למסיבת רווקות,
						  מחיר המבצע הינו למינימום 6 טיפולים, בחדר דיסקרטי ושקט.
						  (יש להזמין טיפולים מראש)	
						</p>
						
						
					</article>
					
					</div>
					<div class="wrapper">
						<article class="col4">
							<a href="index.php" class="link1">חזרה לדף הבית</a>
						</article>
					</div>
				
				
			
				</div></div>
				
			</section>
		</div>
</div></div>
		<div class="main">
			<!--content end-->
			<!--footer -->
			<footer>
				<div class="pad">
					 <a href="index-6.html"></a><br>
					<!-- {%FOOTER_LINK} -->
				</div>
			</footer>
			<!--footer end-->
		</div>
</body>
</html>
