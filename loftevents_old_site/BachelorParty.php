<!DOCTYPE html>
<html lang="en">
<head>
  <title>מסיבת רווקים</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="./css/reset.css" type="text/css" media="all">
  <link rel="stylesheet" href="./css/layout.css" type="text/css" media="all">
  <link rel="stylesheet" href="./css/style.css" type="text/css" media="all">
  <!--[if lt IE 9]>
    <script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
  	<script type="text/javascript" src="js/html5.js"></script>
  <![endif]-->
  <script type="text/javascript" src="./js/jquery.js"></script>
  <script src="./js/jquery-ui.js" type="text/javascript"></script>
  <script src="./js/scroll.js" type="text/javascript"></script>
    <script src="./js/script.js" type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37814978-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    </head>

<body id="page5">
<div class="body1">
			<!--header -->
			
				<div class="main">
				<div class="wrapper">
					<nav>
						<ul id="top_nav">
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li class="bg_none"><a href="#"></a></li>
						</ul>
					</nav>
					<div id="date"></div>
				</div>
				</div>
			<div class="main"><div class="pad bg">
			<header>
			<div class="wrapper">
                    <div id='slogan'>loft events<span>054-2512-512</span></div>
                    <h1><a href="index.php" id="logo"><img src="./images/logo_lofte.png" alt="Events Loft" height="80px" width="190px"/>  <span></span></a></h1>
                </div>
				<nav>
				<ul id="menu">
                        <li><a href="contact.php">&nbsp;&nbsp;צור קשר&nbsp;&nbsp;</a></li>
                        <li><a href="about.php">אודות הLoft</a></li>
                        <li><a href="gallery.php">&nbsp;&nbsp;&nbsp; גלריה &nbsp;&nbsp;&nbsp;</a></li>
                        <li><a href="BacheloretteParty.php">מסיבת רווקות</a></li>
                        <li><a href="BachelorParty.php">מסיבת רווקים</a></li>
                        <li class="bg_none"><a href="index.php">&nbsp;&nbsp;דף הבית&nbsp;&nbsp;</a></li>
                    </ul>
				</nav>
			</header>
			<!--header end-->
			<!--content -->
			<section id="content">
				<div class="pad_bot3"><div class="line1 wrapper">
					<div class="wrapper">
					<article class="col2">
						<h2>מסיבת רווקים</h2>
						<p class="font1">לופט איוונטס תוכנן ועוצב במיוחד למסיבות רווקים ורווקות,
											לופט לאירועים מיוחדים ובלתי נשכחים,
											גברים אתם יכולים להעביר לילה בלטי נשכח, אתם חייבים לדעת לארגן אותו !
											לשרותכם ג'קוזי יוקרתי ומעוצב מלא במים חמים שגורם לכם להתאהב בו,
											לאחר שתתארגנו על שתיה חריפה, מנצ'ס ושהראש יהיה בול כמו שאתן רוצים 
											אתם יכולים ליהנות ממערכת הקריוקי העשירה עם מגוון עצום של שירים ופלייבקים,
											לופט איוונטס דאג לעזור לכם באירגון מסיבת רווקים.
						</p>
						<p class="color1">טיפים למסיבות רווקים,
							מסיבות רווקים אמורות לחגוג את הקץ לחיי הרווקות של החתן לפניי שהוא מתמסד לאישה אחת ואומר שלום לחיי הרווקות. חייבים לדאוג שאירוע מסוג זה יתוכנן כהלכה וישאיר רושם לאורך זמן. אם זה חבר קרוב או חבר משפחה זאת אחריותך בלבד להפיק את מסיבת הרווקים שתתאים לאופי החתן.
							תכנון מסיבת רווקים יכולה להיות משימה קשה במיוחד כאשר מדובר בכאלה שלא בקיאים בהפקת אירועים שלא כמו רוב הבחורות. קיבצנו עבורכם כמה טיפים חשובים לתכנון מסיבת רווקים כדי להבטיח מסיבה מוצלחת ולילה שייחקק בזיכרונם של החתן וחבריו.
							באופן מסורתי מסיבת רווקים נערכת לילה לפני יום החתונה אבל זה לא המקרה בימינו, היום שלפני החתונה מוקדש בדרך כלל להכנות שלפני החתונה, לכן עדיף לתכנן את מסיבת הרווקים בערך שבוע או יותר לפני יום החתונה. בנוסף חייבים לוודות שכל מוזמני המסיבה יפנו את לוח הזמנים ביום מסיבת הרווקים. אם יש כאלה שמגיעים מרחוק עדיף ליידע אותם מוקדם יותר בכדיי שיגיעו בזמן. עדיף לתכנן את האירוע באחד מסופיי השבוע .
							לפני שמתחילים לחשוב על רעיונות למסיבת רווקים, חשוב ראשית לקבוע את התקציב שלכם לאירוע. בדרך כלל זהו חברו הטוב של החתן שמארגן את האירוע אבל מקובל שכל אורחי האירוע השתתפו בתשלומים. אתה יכול להיות בטוח שכל חבריי החתן הקרובים ישמחו לתרום את חלקם. משפחת החתן, חבריו לחיים וחבריו לעבודה הם אורחים רצויים למסיבת הרווקים. קח בחשבון כמה הם יכולים לתת בשביל האירוע. ההוצאות הכלליות בדרך כלל מסתכמות באוכל, שתייה, תחבורה ופעילויות נוספות. תמנה את כולם ברשימה מסודרת לשם הכנת תקציב מדויק ככול האפשר: סוג המסיבה, הקונספט, מספר האורחים בדרך כלל יכולים לתת מושג כללי על כמה האירוע הולך לעלות.
							חשוב ליידע את החתן בקשר לרשימת האורחים,  עדיף שירגיש בנוח עם הקהל המוזמן ובנוסף שאורחי האירוע ירגישו בנוח אחד עם השני. בצורה כזו כולם ירגישו בנוח אחד עם השני וייהנו יותר. בנוסף אפשר לשאול חבריי משפחה וחברים טובים אם הם מעוניינים להוסיף  אנשים לרשימת האורחים. אתה גם יכול להזמין חברים שגרים מחוץ לעיר. מספר האורחים משפיע רבות על התקציב לכן עדיף לשקול לצמצם את רשימת האורחים אם אתם חושבים שתחרגו מהתקציב שנקבע.
							בסופו של דבר תחליט על קונספט מתאים למסיבת הרווקים בהתאם לאופיו ואישיותו של החתן. לכן בתור חברו הטוב של החתן אתה חייב להפיק את מסיבת הרווקים המושלמת לעיניי החתן המיוחל.
							חשפניות: קצת קשה לקרוא למסיבת רווקים ללא חשפנית-"מסיבת רווקים" 
							היום תופעת החשפניות במסיבות רווקים מקובלת ולא נתפסת כדבר שלילי. קיימות סוכנויות חשפנות המקיימות אירועים בסטנדרטים גבוהים. לרוב תהיה אפשרות לבחור באתרי האינטרנט חשפנית מתוך מאגר תמונות.
						</p>
						<p class="pad_bot1">כל הסודות והטיפים לקניית אלכוהול משובח
							אלכוהול יש בכל מסיבת רווקים, מה שהופך אותו לאחד השחקנים הראשיים של הערב. הצלחתן של מסיבות רווקים תלויה רבות בסוג, באיכות, ובכמות האלכוהול שתביאו. אז בואו נסביר לכם איך עושים את זה כמו שצריך.
							 אולי זו העובדה שבסך הכל מדובר בנוזל שקוף, או סתם מתוך רצון חבוי לחסוך, אבל יש אנשים שטוענים שאיכות האלכוהול זו סתם פיקציה, שרק מומחים מבדילים בין הטעם של וודקה זולה לאיכותית. לגבי הטעם אולי, אבל לגבי ההרגשה באותו הערב ובעיקר ביום שאחרי, ממש לא.  איכות האלכוהול היא קריטית להצלחתה של מסיבת רווקים, אחרת אתם עלולים להתרכז יותר בתפיסת השיער של החברים שלכם ובניקוי הרצפה. בין הזול ביותר ליקר ביותר יש אזור ביניים רחב למדיי עם איכות טובה במחירים שפויים. ובהנחה שאתם עדיין צעירים שלא שוחים בכסף, זה בדיוק האזור שלכם.
							כששותים אלכוהול בכלל ובמהלך מסיבת רווקים בפרט, חשוב לא לערבב בין סוגים שונים, אך כשמביאים 20 איש למסיבת רווקים, כל אחד אוהב משהו אחר. דאגו ל"נציגות" לכמה שיותר משפחות אלכוהול – וודקה, וויסקי, ליקרים, יינות ושמפניה. כדי שלא יישאר אחד שלא יהיה לו מה לשתות. בהערכות כמות האלכוהול עבור מסיבה המונית קיים אצלכם יתרון. אתם קונים הרבה, ולכן ניתן לקבל הנחות מחיר משמעותיות.
							באופן גורף, חנויות ורשתות האלכוהול והליקר זולות משמעותית מהסופרים השונים ובוודאי מהפיצוציות. גם אתרי אינטרנט שונים עשויים להיות אופציה טובה. בכל מקרה, לקראת כל מסיבת רווקים ערכו רשימת אלכוהול מסודרת, קבלו הצעת מחיר מכמה מקומות (אבל לא חנויות קטנות ושכוחות אל, שרק אלוהים יודע אם האלכוהול שלהם אמיתי) ובחרו באופציה הזולה ביותר. לכו על אחת הרשתות המוכרות וודאו את קיומם של אמצעי הזיהוי למניעת זיוף.
							 מסיבת רווקים בלי אלכוהול וחשפניות טוב היא כמו חתונה בלי אוכל טוב. משאירה טעם רע בפה... בחירה שתיעשה בקפידה תשדרג לכם את כל הערב.  תבלו!
							מומלץ להזמין מופע חשפנות ברמה איכותית מאחר וזה מסמר הערב.</p>
						
						
					</article>
					 
					<article class="col1 pad_left1">
					<!-- 
						<h2>טיפים לארוע</h2>
						<ul class="list1 pad_bot1">
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
						</ul>
						-->
					</article>
					
					</div>
					<div class="wrapper">
						<article class="col2">
							<a href="#" class="link1"></a>
						</article>
						<article class="col1 pad_left1">
							<a href="#" class="link1"></a>
						</article>
					</div>
				</div></div>
				<div class="wrapper pad_bot2">
					<h2>הגלריה</h2>
					<dl class="folio">
					 	<dt><img alt="" src="./images/man1.jpg" id="largeImg"/></dt>
					 	<dd>
							<div class="sliderGallery">
								<ul>
										<li class="marg_right1"><a href="./images/man1.jpg"><img alt="" src="./images/man1b.jpg" /></a></li>
										<li class="marg_right1"><a href="./images/man2.jpg"><img alt="" src="./images/man2b.jpg" /></a></li>
										<li class="marg_right1"><a href="./images/man3.jpg"><img alt="" src="./images/man3b.jpg" /></a></li>
										<li class="marg_right1"><a href="./images/man11.jpg"><img alt="" src="./images/man11b.jpg" /></a></li>
										<li class="marg_right1"><a href="./images/man4.jpg"><img alt="" src="./images/man4b.jpg" /></a></li>
										<li class="marg_right1"><a href="./images/man5.jpg"><img alt="" src="./images/man5b.jpg" /></a></li>
										<li class="marg_right1"><a href="./images/man6.jpg"><img alt="" src="./images/man6b.jpg" /></a></li>
										<li class="marg_right1"><a href="./images/man7.jpg"><img alt="" src="./images/man7b.jpg" /></a></li>
										<li class="marg_right1"><a href="./images/man8.jpg"><img alt="" src="./images/man8b.jpg" /></a></li>
										<li class="marg_right1"><a href="./images/man9.jpg"><img alt="" src="./images/man9b.jpg" /></a></li>
								</ul>
								<div class="slider">
									<div class="handle"></div>
								</div>

							</div>
						</dd>
					</dl>

				</div>
			</section>
		</div>
</div></div>
		<div class="main">
			<!--content end-->
			<!--footer -->
			<footer>
				<div class="pad">
					<a href="index-6.html"></a><br>
					<!-- {%FOOTER_LINK} -->
				</div>
			</footer>
			<!--footer end-->
		</div>
</body>
</html>
