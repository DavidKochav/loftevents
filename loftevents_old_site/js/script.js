
/* gallery2 page folio */

$(document).ready(function(){
	
	$(".folio ul li a").click(function(){
									   
  	var largePath = $(this).attr("href");
  	$("#largeImg").animate({ opacity: "0" }, "middle",function(){
   		 $("#largeImg").attr({ src: largePath})
		 })
	$("#largeImg").animate({ opacity: "1" }, "slow")
      return false;
   })
});

/* looped slider */	
	$(function(){
	        $('#loopedSlider').loopedSlider();
	    });

/* gallery1 page home */
		$(function() {
		 $(".carousel").jCarouselLite({
			  btnNext: ".next",
			  btnPrev: ".prev"
		 });
	});


/* to gallery1 animated box */	
	jQuery(document).ready(function(){
    jQuery(".carousel li").hover(function(){
    jQuery(this).find(".box").stop().animate({right:"0"}, "middle")
      }, function (){
      jQuery(this).find(".box").stop().animate({right:"-81"}, "middle")
      });
    });
