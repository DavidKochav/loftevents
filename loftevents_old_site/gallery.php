<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Loft Events</title>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ultra">
<link rel="stylesheet" type="text/css" href="./js_gallery/vegas/jquery.vegas.css">
<link rel="stylesheet" type="text/css" href="./js_gallery/jscrollpane/jquery.jscrollpane.css">
<link rel="stylesheet" type="text/css" href="./css_gallery/styles.css">
<script src="http://code.jquery.com/jquery-1.6.2.min.js"></script>
<script src="./js_gallery/jquery.easing.js"></script>
<script src="./js_gallery/vegas/jquery.vegas.js"></script>
<script src="./js_gallery/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="./js_gallery/buzz/buzz.js"></script>
<script src="./js_gallery/gallery.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37814978-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
    <div id="flash"></div>
    <div id="title">
        <h1>New York Gallery</h1>
        <p>Loft Event <a href="http://www.loftevents.co.il">Loft Events</a></p>
    </div>
    <div id="thumbnails">
        <ul>
            <li><a href="./img_gallery/01.jpg"><img src="./img_gallery/01b.jpg" title="Loft" data-valign="top"></a></li>
            <li><a href="./img_gallery/02.jpg"><img src="./img_gallery/02b.jpg" title="Loft Event" data-valign="bottom"></a></li>
            <li><a href="./img_gallery/03.jpg"><img src="./img_gallery/03b.jpg" title="Bar" data-valign="bottom"></a></li>
            <li><a href="./img_gallery/04.jpg"><img src="./img_gallery/04b.jpg" title="Jacuzzi" data-valign="bottom"></a></li>
            <li><a href="./img_gallery/05.jpg"><img src="./img_gallery/05b.jpg" title="Space" data-valign="bottom"></a></li>
            <li><a href="./img_gallery/06.jpg"><img src="./img_gallery/06b.jpg" title="Fun" data-valign="bottom"></a></li>
            <li><a href="./img_gallery/07.jpg"><img src="./img_gallery/07b.jpg" title="Quality" data-valign="bottom"></a></li>
            <li><a href="./img_gallery/08.jpg"><img src="./img_gallery/08b.jpg" title="style Villa" data-valign="bottom"></a></li>
            <li><a href="./img_gallery/09.jpg"><img src="./img_gallery/09b.jpg" title="LCD" data-valign="bottom"></a></li>
            <li><a href="./img_gallery/10.jpg"><img src="./img_gallery/10b.jpg" title="karaoke" data-valign="bottom"></a></li>
            <li><a href="./img_gallery/11.jpg"><img src="./img_gallery/11b.jpg" title="Shower" data-valign="bottom"></a></li>
            <li><a href="./img_gallery/12.jpg"><img src="./img_gallery/12b.jpg" title="Games" data-valign="bottom"></a></li>
              
            
            
        </ul>
    	<div id="pointer"></div>
    </div>
    <div id="pause"><a href="#">Paused</a></div>
    <div id="volume" class="all"><a href="#">Sounds</a></div>
</body>
</html>
