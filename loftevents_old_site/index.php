<!DOCTYPE html>
<html lang="en">
<head>
  <title>loft events</title>
  <meta charset="utf-8">
  
  
  <link rel="stylesheet" type="text/css" href="./css/reset.css"  media="all">
  <link rel="stylesheet" type="text/css" href="./css/layout.css"  media="all">
  <link rel="stylesheet" type="text/css" href="./css/style.css"  media="all">
  
  <link rel="stylesheet" type="text/css" href="./css/slicebox.css" />
  <link rel="stylesheet" type="text/css" href="./css/custom.css" />
   
  
  
  <script type="text/javascript" src="./js/modernizr.custom.46884.js"></script>
  
   
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  <script type="text/javascript" src="./js/jquery.slicebox.js"></script>
  <script src="./js/loopedslider.0.5.4.js" type="text/javascript"></script>
  <script src="./js/jcarousellite.js" type="text/javascript"></script>
  <script src="./js/script.js" type="text/javascript"></script>
  
  

  
  
		
		
  <!--[if lt IE 9]>
    <script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
      <script type="text/javascript" src="js/html5.js"></script>
  <![endif]-->
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37814978-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body id="page1">
<div class="body1">
            <!--header -->
            
                <div class="main">
                <div class="wrapper">
                    <nav>
                        <ul id="top_nav">
                            <li><a href="#"></a></li>
                            <li><a href="#"></a></li>
                            <li><a href="#"></a></li>
                            <li class="bg_none"><a href="#"></a></li>
                        </ul>
                    </nav>
                    <div id="date"></div>
                </div>
                </div>
           
            <div class="main">
            <div class="pad bg">
            <header>
            
                <div class="wrapper">
                    <div id='slogan'>loft events<span>054-2512-512</span></div>
                    <h1><a href="index.php" id="logo"><img src="./images/logo_lofte.png" alt="Events Loft" height="80px" width="190px"/>  <span></span></a></h1>
                </div>
                <nav>
                    <ul id="menu">
                        <li><a href="contact.php">&nbsp;&nbsp;צור קשר&nbsp;&nbsp;</a></li>
                        <li><a href="about.php">אודות הLoft</a></li>
                        <li><a href="gallery.php">&nbsp;&nbsp;&nbsp; גלריה &nbsp;&nbsp;&nbsp;</a></li>
                        <li><a href="BacheloretteParty.php">מסיבת רווקות</a></li>
                        <li><a href="BachelorParty.php">מסיבת רווקים</a></li>
                        <li class="bg_none"><a href="index.php">&nbsp;&nbsp;דף הבית&nbsp;&nbsp;</a></li>
                    </ul>
                </nav>
                <div class="container">
	               <div class="wrapper">
	                   <ul id="sb-slider" class="sb-slider">
	                      <li>
	                       <a href="#"><img src="./images/img1.jpg" alt=""></a>
	                         <div class="sb-description">
								<h3>  &nbsp; Loft Events</h3>
							</div>
	                       </li>
	                       <li>
	                       	<a href="#"><img src="./images/img2.jpg" alt=""></a>
	                          <div class="sb-description">
								<h3>  &nbsp; style Villa</h3>
								</div>
	                        </li>
	                        <li>
	                          <a href="#"> <img src="./images/img3.jpg" alt=""></a>
	                           <div class="sb-description">
									<h3>  &nbsp; LCD and BAR</h3>
								</div>
	                        </li>
	                       	<li>
	                         <a href="#"> <img src="./images/img4.jpg" alt=""></a>
	                           <div class="sb-description">
								<h3>  &nbsp; play station</h3>
								</div>
	                         </li>
	                         <li>
	                          <a href="#"><img src="./images/img5.jpg" alt=""></a>
	                           <div class="sb-description">
									<h3>  &nbsp; Jacuzzi</h3>
								</div>
	                         </li>
	                         <li>
	                         	<a href="#"> <img src="./images/img6.jpg" alt=""></a>
	                              <div class="sb-description">
									<h3>  &nbsp; Space</h3>
								</div>
	                          </li>
	                          <li>
	                            <a href="#"> <img src="./images/img7.jpg" alt=""></a>
	                            <div class="sb-description">
									<h3>  &nbsp; Quality</h3>
								</div>
	                           </li>
	                           <li>
	                             <a href="#"> <img src="./images/img8.jpg" alt=""></a>
	                             <div class="sb-description">
									<h3>  &nbsp; karaoke</h3>
								 </div>
	                            </li>
	                         </ul>
	                         <div id="shadow" class="shadow"></div>
	                                      
	                         <div id="nav-arrows" class="nav-arrows">
								<a href="#"></a>
								<a href="#"></a>
							</div>
							<div id="nav-options" class="nav-options">
								<a> <span id="navPlay" style="background: #cbbfae url(./images/play.png) no-repeat center;">Play</span></a>
								<a><span id="navPause" style="background: #cbbfae url(./images/pause.png) no-repeat center;">Pause</span></a>
							</div>
	            </div>
            </div> 
            </header>
            <!--header end-->
            <!--content -->
            
            <section id="content">
                <div class="line1 wrapper">
                    <article class="col2">
                        <h2 class="hello"><b></b></h2>
                        <div class="wrapper pad_bot1">
                            <figure class="left marg_right1"><img alt="" src="./images/galleryImg1.jpg"></figure>
                            <figure class="left"><img alt="" src="./images/galleryImg2.jpg"></figure>
		                        </div>
		                        <p class="font1">לופט איוונטס לאירועים יוקרתיים ויחודיים.</p>
                        
                        <p class="color1 pad_bot1">לופט ששטחו כ-100 מטרים, המכיל עד 40 אנשים !
													לופט איוונס מיועד לכל מסיבה ולכל מי שמחפש מקום מעוצב ואיכותי לחגוג בו.
													הלופט ממוקם באיזור התעשיה בבת-ים, מקום דיסקרטי ושקט ניתן להשמיע מוזיקה 
													כל שעות הפעילות ללא הפרעה וטענה.
													בכל אירוע הלופט מאובטח בשומר.
													שטח חניה גדול ומואר ללא תשלום, וממש צמוד ללופט.
													לופט איוונטס מעוצב ומאובזר ברמה הכי גבוהה שקיימת !!!
                         </p>
                         
                        <div class="pad_bot1"><a href="about" class="link1">קרא עוד</a></div>
                        
                        <h2 style="direction:ltr">LoftEvents</h2>
                        
                                      <div class="carousel-box">
                           <div class="inner">
                              <a href="#" class="prev"></a>
                              <a href="#" class="next"></a>
                              <div class="carousel">
                                 <ul>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC1.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC2.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC3.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC4.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC5.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC6.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC7.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC8.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC9.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC10.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                    <li>
                                        <a href="gallery.php"><img src="./images/DSC11.jpg" alt="" width="121" height="171" /></a>
                                        <a href="gallery.php" class="box">view<br> more</a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <p class="color1 pad_bot1">מערכת קריוקי מקצועית, תיקייה עם 3000 שירי קריוקי, זוג מקרופונים אלחוטיים ואיכותיים,
													מטבחון שירות הכולל, תמי 4 (מים קרים וחמים) מיקרוגל, מקרר גדול לאחסון, שיש ושולחן עבודה.
                        </p>
                        <div class="pad_bot2" ><a href="gallery.php" class="link1">לגלריה המלאה</a></div>
                    </article>
                    <article class="col1 pad_left1">
<!--                        <h2>--><?php //echo $this->lang->line('contctSubject');?><!--</h2>-->
                        <ul class="pad_bot1">
                            <li class="wrapper under">
                                <figure class="left marg_right1"><img alt="" src="./images/side2.jpg"></figure>
                                <a href="#">פלזמה וקונסולת משחקים פלייסטיישן</a><br>
                                		לופט איוונטס מאובזר בפלזמה 50 אינץ גדולה בסלון, 
										FULL HD עם כל ערוצי ה-YES פתוחים כולל חבילת הכדורגל
										בנוסף מחובר לפלזמה קונסולת משחקים של פלייסטיישן 3
										עם המשחק שמשגע אותו וגורם לנו לשאטים בהתערבויות,
										Pro evolution soccer 2012 זוג ג'וייסטיקים אלחוטיים,
										ברצלונה ורק ניצחונות !!!
                                
                                
                            </li>
                            <li class="wrapper under">
                                <figure class="left marg_right1"><img alt="" src="./images/side1.jpg"></figure>
                                <a href="#">מערכת ישיבה יוקרתית ונוחה</a><br>
                              מערכת ישיבה יוקרתית ונוחה, ממוקמת במרכז העניינים, 
								כולל שולחן סלוני מרווח, מסך פלזמה 50" ומסך מחשב 
								לעריכת שירים.
								
                               </li>
                            <li class="wrapper under">
                                <figure class="left marg_right1"><img alt="" src="./images/side3.jpg"></figure>
                                <a href="#">בר אקטיבי </a><br>	
								 בר אקטיבי מעוצב עם 6 כיסאות בר מעוצבות ונוחות במיוחד
								                                		
                            </li>
                            <li class="wrapper under">
                                <figure class="left marg_right1"><img alt="" src="./images/side4.jpg"></figure>
                                <a href="#">ג'קוזי יוקרתי</a><br>
								ג'קוזי יוקרתי מבית FFERA חברה מובילה בתחום הבריכות ספא,
								ג'קוזי מפנק ואיכותי, 8 מקומות ישיבה מרווחים,
								מפלי מים,  תאורת לדים, גוף חימום השומר על טמפרטורת המים,
								קביעת טמפרטורה על צג דיגיטלי, קל לתפעול.
								הג'קוזי מתוחזק ומטופל ברמה הגבוהה ביותר.
								המים בג'קוזי מוחלפים כל אחרי אירוע !!!
                                
                             </li>             
                             <li class="wrapper">
                                <figure class="left marg_right1"><img alt="" src="./images/side5.jpg"></figure>
                                <a href="#">שירותים ומקלחת</a><br>
								אחד הדברים העיקריים שלופט איוונטס שם דגש הוא
								האסטטיקה והניקיון,
								בלופט שתי יחידות שירותים מרווחים מעוצבים ונעימים
								מקלחת מרווחת ונקייה,כולל ארון אמבטיה צף עם כיור 
								מונח ומעוצב,
								ריח טוב ונעים לאורך כל הזמן.   
                             </li>
                        </ul>
                        <div class="pad_bot1"><a href="#" class="link1"></a></div>
                    </article>
                </div>
                
            </section>
        </div>
</div></div>
        <div class="main">
            <!--content end-->
            <!--footer -->
            <footer>
                <div class="pad">
                      <a href="index-6.html"></a><br>
                    <!-- {%FOOTER_LINK} -->
                </div>
            </footer>
            <!--footer end-->
        </div>
        
		
		
		<script type="text/javascript">
			$(function() {
				
				var Page = (function() {

					var $navArrows = $( '#nav-arrows' ).hide(),
						$navOptions = $( '#nav-options' ).hide(),
						$shadow = $( '#shadow' ).hide(),
						slicebox = $( '#sb-slider' ).slicebox( {
							onReady : function() {

								$navArrows.show();
								$navOptions.show();
								$shadow.show();

							},
							orientation : 'h',
							cuboidsCount : 3
						} ),
						
						init = function() {

							initEvents();
							
						},
						initEvents = function() {

							// add navigation events
							$navArrows.children( ':first' ).on( 'click', function() {

								slicebox.next();
								return false;

							} );

							$navArrows.children( ':last' ).on( 'click', function() {
								
								slicebox.previous();
								return false;

							} );

							$( '#navPlay' ).on( 'click', function() {
								
								slicebox.play();
								return false;

							} );

							$( '#navPause' ).on( 'click', function() {
								
								slicebox.pause();
								return false;

							} );

						};

						return { init : init };

				})();

				Page.init();

			});
		</script>
</body>

</html>
