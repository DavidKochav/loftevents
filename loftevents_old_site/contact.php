<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="./css/reset.css" type="text/css" media="all">
  <link rel="stylesheet" href="./css/layout.css" type="text/css" media="all">
  <link rel="stylesheet" href="./css/style.css" type="text/css" media="all">
  <!--[if lt IE 9]>
    <script type="text/javascript" src="http://info.template-help.com/files/ie6_warning/ie6_script_other.js"></script>
  	<script type="text/javascript" src="js/html5.js"></script>
  <![endif]-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37814978-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body id="page6">
<div class="body1">
			<!--header -->
			
				<div class="main">
				<div class="wrapper">
					<nav>
						<ul id="top_nav">
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li><a href="#"></a></li>
							<li class="bg_none"><a href="#"></a></li>
						</ul>
					</nav>
					<div id="date"></div>
				</div>
				</div>
			<div class="main"><div class="pad bg">
			<header>
				<div class="wrapper">
                    <div id='slogan'>loft events<span>054-2512-512</span></div>
                    <h1><a href="index.php" id="logo"><img src="./images/logo_lofte.png" alt="Events Loft" height="80px" width="190px"/>  <span></span></a></h1>
                </div>
				<nav>
					<ul id="menu">
                        <li><a href="contact.php">&nbsp;&nbsp;צור קשר&nbsp;&nbsp;</a></li>
                        <li><a href="about.php">אודות הLoft</a></li>
                        <li><a href="gallery.php">&nbsp;&nbsp;&nbsp; גלריה &nbsp;&nbsp;&nbsp;</a></li>
                        <li><a href="BacheloretteParty.php">מסיבת רווקות</a></li>
                        <li><a href="BachelorParty.php">מסיבת רווקים</a></li>
                        <li class="bg_none"><a href="index.php">&nbsp;&nbsp;דף הבית&nbsp;&nbsp;</a></li>
                    </ul>
				</nav>
			</header>
			<!--header end-->
			<!--content -->
			<section id="content">
				<div class="pad_bot3"><div class="line2"><div class="line1 wrapper">
					<div class="wrapper">
						<article class="col1">
							<h2>פרטי יצירת קשר</h2>
							<p>ליצירת קשר ניתן לפנות אלנו באחת מהדרכים הבאות או דרך איזור הפניות באתר.</p>
						<!-- 	<figure class="pad_bot1"><img alt="" src="images/page6_img1.jpg"></figure>  -->
							<p><span class="color1">לופט אוונטס .</span><br>
									אהוד קנמון 3 (איזור התעשיה), 59601 בת-ים.</p>
							<p class="pad_bot1">           
						       <span class="right marg_right1"> טלפון: </span> &nbsp; 054-251-2512<br>
						       <span class="right marg_right1">פקס:</span>  &nbsp; 03-6560433 <br>
							  <span class="right marg_right1">אימייל: <a href="mailto:">loftevents.batyam@gmail.com</a></span>
						</p>
							
							
						</article>
						<article class="col3 pad_left1">
							<h2>פניות</h2>
							<form method="post" action=contact.php"  id="ContactForm" >
							<div>
									<input class="input" type="text" name="customer_name" value="רשום שם ומשפחה:"  onblur="if(this.value=='') this.value='חובה לרשום שם פרטי ומשפחה:'" onFocus="if(this.value =='Enter Your Name:' ) this.value=''"   />
									<input class="input" type="text" name="email" value="רשום דואר אלקטרוני:"  onblur="if(this.value=='') this.value='נא רשום את הדואר האלקטרוני שלך (email):'" onFocus="if(this.value =='Enter Your E-mail:' ) this.value=''"   />
								<input class="input" type="text" name="phone" value="טלפון לחזרה:"  onblur="if(this.value=='') this.value='חובה לרשום מספר טלפון:'" onFocus="if(this.value =='Enter Your State:' ) this.value=''"   />
								<textarea cols="1" rows="1" name="message" onBlur="if(this.value=='') this.value='חובה לרשום הודעה:'" onFocus="if(this.value =='Enter Your Message:' ) this.value=''"  >הודעה:</textarea>
							<!-- 	<a href="#" class="link1" onClick="document.getElementById('ContactForm').submit()">שלח</a>  -->
								<input type="hidden" value="customer" name="send_message">
								<input type="submit" value="שלח" class="link1" onClick="document.getElementById('ContactForm').submit()" />
								<!--  <a href="#" class="link1" onClick="document.getElementById('ContactForm').reset()">Clear</a> -->
							</div>
							</form>
							
						</article>
						<article class="col1 pad_left1">
							<h2>מיקומנו</h2>
							<figure class="pad_bot1"><img alt="" src="./images/page6_img2.jpg"></figure>
							<p class="color1">איזור התעשיה , רחוב אהוד קנמון 10 בבת-ים</p>
							<p>בזכות אופי האזור ניתן לחגוג עם מוסיקה גבוהה אשר לא תפריע לסובבים, הלופט עם מרחב גדול ויכול להכיל עד 40 אנשים</p>
							ישנה חניה  גדולה ומוארת צמוד ללופט<br>
						</article>
					</div>
					
				</div></div></div>

				
				
			</section>
		</div>
</div></div>
		<div class="main">
			<!--content end-->
			<!--footer -->
			<footer>
				<div class="pad">
					 <a href="index-6.html"></a><br>
					<!-- {%FOOTER_LINK} -->
				</div>
			</footer>
			<!--footer end-->
		</div>
</body>
</html>
